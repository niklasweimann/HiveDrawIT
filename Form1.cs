﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hive_cheating_tool
{
    public partial class Form1 : Form
    {
        string path;

        public Form1()
        {
            InitializeComponent();
        }

        private void dateiEinlesenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Text Datei|*.txt";
            openFileDialog1.Title = "Wähle eine Textquelle";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                path = openFileDialog1.FileName;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void hilfeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 infoform = new AboutBox1();
            infoform.Show();
        }

        private void aktualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (path != null)
            {

                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        listBox1.Items.Add(line);
                    }
                }
            }
            else
            {

                MessageBox.Show("Leider hast du keine Datei mit Wörtern ausgewählt.","Liste Fehlt",MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            listBox1.Items.Clear();

            int length = Convert.ToInt32(textBox1.Text);

            if (path != null)
            {

                using (StreamReader sr = new StreamReader(path))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Length == length)
                        {
                            listBox1.Items.Add(line);
                        }
                    }
                }
            }
            else
            {

                MessageBox.Show("Leider hast du keine Datei mit Wörtern ausgewählt.", "Liste Fehlt", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            int length = Convert.ToInt32(textBox1.Text);
            length--;
            textBox1.Text = Convert.ToString(length);
        }

        private void plusButton_Click(object sender, EventArgs e)
        {
            int length = Convert.ToInt32(textBox1.Text);
            length++;
            textBox1.Text = Convert.ToString(length);
        }
    }
}
